<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Styleguide</title>
	<link rel="stylesheet" href="css/main.css">
	<script src="js/app.js"></script>
</head>
<body>
	
	<h1 class="furniture">Styleguide</h1>

	<?php
		$components = glob("components/*.php");
		foreach ($components as $comp){
			$title = explode('.', $comp);
        	echo '<h2 class="furniture">'.ucwords($title[1]).'</h2>';
        	echo '<div class="demo">';
        	include "$comp";
        	echo '</div>';
		}

	?>
	
</body>
</html>