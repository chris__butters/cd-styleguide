var gulp = require('gulp'),
sass = require('gulp-sass'),
sassGlob = require('gulp-sass-glob'),
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
cmq = require('gulp-group-css-media-queries'),
cssmin = require('gulp-cssmin'),
rename = require("gulp-rename"),
autoprefixer = require('gulp-autoprefixer'),
sourcemaps = require('gulp-sourcemaps');

gulp.task('scripts', function () {

    // Edit the source to add addition javascript files that will compile into plugin.min.js
    gulp.src('js/src/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('app.js')) //the name of the resulting file
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('js'));

    gulp.src('components/src/*.js')
        .pipe(sourcemaps.init())
        .pipe(concat('demo.js')) //the name of the resulting file
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('js'));

      
});

gulp.task('styles', function () {
    gulp.src('css/src/main.scss')
      .pipe(sassGlob())
      // .pipe(sourcemaps.init())
      .pipe(sass())
      .on('error', swallowError)
      //.pipe(gulp.dest('css'))
      .pipe(autoprefixer({
          browsers: ['last 2 versions'],
          cascade: true
      }))
      // .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('css'));

});

gulp.task('default',['scripts','styles'], function(){});

gulp.task('watch', function() {
  // gulp.watch('css/src/**/*.scss', ['styles']);
  gulp.watch('css/src/**/**/*.scss', ['styles']);
  gulp.watch('components/css/**/*.scss', ['styles']);

  gulp.watch('components/js/*.js', ['scripts']);
  gulp.watch('js/src/*.js', ['scripts']);
});

// Custom error function to stop emit(end);
var swallowError = function(error) {
    console.log(error.toString());
}
